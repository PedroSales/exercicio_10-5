package Controller;

import Model.ContaCorrente;
import Model.ContaPoupanca;

public class ControleContas {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		
		
		ContaCorrente Conta1 = new ContaCorrente(0);
		ContaPoupanca Conta2 = new ContaPoupanca(0);
		
		
		Conta1.deposita(500);
		Conta2.deposita(600);
		
		
		Conta1.saca(100);
		Conta2.saca(150);
		
		
		Conta1.atualiza(0.2);
		Conta2.atualiza(0.2);
		
		System.out.println("Saldo Conta-Corrente: " + Conta1.getSaldo());
		System.out.println("Saldo Conta-Poupança: " + Conta2.getSaldo());
		
		
		
	}

}
