package Model;

public class ContaPoupança extends Conta {

	private String numeroCP;
	private String agenciaCP;
	
	public ContaPoupança (double saldo){
		super(saldo);
	}

	public String getNumeroCP() {
		return numeroCP;
	}

	public void setNumeroCP(String numeroCP) {
		this.numeroCP = numeroCP;
	}

	public String getAgenciaCP() {
		return agenciaCP;
	}

	public void setAgenciaCP(String agenciaCP) {
		this.agenciaCP = agenciaCP;
	}
	
	public void atualizaCP (double taxa){
		double saldo = getSaldo();
		saldo += taxa*saldo;
		setSaldo(saldo);
	}

}
