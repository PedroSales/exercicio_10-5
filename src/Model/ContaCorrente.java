package Model;

import Interface.Tributavel;

public class ContaCorrente extends Conta implements Tributavel {

	private String numeroCC;
	private String agenciaCC;
	
	public ContaCorrente (double saldo){
		super(saldo);
	}

	public String getNumeroCC() {
		return numeroCC;
	}

	public void setNumeroCC(String numeroCC) {
		this.numeroCC = numeroCC;
	}

	public String getAgenciaCC() {
		return agenciaCC;
	}

	public void setAgenciaCC(String agenciaCC) {
		this.agenciaCC = agenciaCC;
	}
	
	
	public void atualiza (double taxa){
		double saldo = getSaldo();
		saldo -= taxa*saldo;
		setSaldo(saldo);
	}

	public double calculaTributos() {
		return this.getSaldo() * 0.01;
	}
	
}
