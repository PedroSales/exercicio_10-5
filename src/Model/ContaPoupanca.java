package Model;

public class ContaPoupanca extends Conta {

	private String numeroCP;
	private String agenciaCP;
	
	public ContaPoupanca (double saldo){
		super(saldo);
	}

	public String getNumeroCP() {
		return numeroCP;
	}

	public void setNumeroCP(String numeroCP) {
		this.numeroCP = numeroCP;
	}

	public String getAgenciaCP() {
		return agenciaCP;
	}

	public void setAgenciaCP(String agenciaCP) {
		this.agenciaCP = agenciaCP;
	}
	
	public void atualiza (double taxa){
		double saldo = getSaldo();
		saldo += taxa*saldo;
		setSaldo(saldo);
	}
	
	

}
