package Model;

import Interface.Tributavel;

public class TestaTributavel {

	
	public static void main(String[] args) {

		ContaCorrente cc = new ContaCorrente(0);
		cc.deposita(100);
		System.out.println(cc.calculaTributos());
		
		
		
		Tributavel t = cc;
		System.out.println(t.calculaTributos());
		System.out.println(t.getSaldo());
		
		//não é possível chamar o método getSaldo() pois tal método não está definido em Tributavel.
	}

}
